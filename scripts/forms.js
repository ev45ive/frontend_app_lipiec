// Czekamy na załadowanie DOM 
window.addEventListener('load', function(){

    // Znajdz formularz
    var form = document.querySelector('form')

    // Walidacja na keyup lub click
    form.addEventListener('keyup', function (event) {
        validate(form, event.target);
    })
    form.addEventListener('click', function (event) {
        validate(form, event.target);
    })

    // Dodanie uzytkownika na przycisk Submit
    form.querySelector('input[type=submit]').addEventListener('click', function (event) {

        // prevent default browser action 
        // before any actions or errors
        event.preventDefault()

        if (form.checkValidity()) {
            var user = getFormData(form);
            appendUser(user)
        } else {
            validate(form)
        }
    })

})


function validate(form, input) {
    // dla kazdego pola
    // metody sa CamelCase - CaseSensitive 

    var fields = input ? [input] : form.querySelectorAll('input, textarea, select');

    fields.forEach(function (element) {

        if (element.checkValidity() == false) {
            // .form-group  dodaj klase .has-error
            element.parentElement.classList.add('has-error')
        } else {
            // usun klase .has-error
            element.parentElement.classList.remove('has-error')
        }
        //  console.log(element.name,element.checkValidity())
    })
}

function getFormData(form) {
    var elements = Array.prototype.slice.apply(form.elements);
    var user = {}

    elements.forEach(function (elem) {
        // if( elem.type in ["radio","checkbox"] && !elem.checked ){
        //      return
        // }
        // user[elem.name] = elem.value;

        //////////////////////////////

        switch (elem.type) {

            case "radio":
            case "checkbox":
                if (elem.checked) {
                    user[elem.name] = elem.value;
                }
                break;
            default:
                user[elem.name] = elem.value;
        }
        //  console.log(elem.name, elem.value)
    });
    return user
}


function appendUser(user) {
    // stworz wiersz - <tr>
    var tr = document.createElement('tr')

    // wypelnij wiersz danymi
    // !!! XSS CODE INJECTION: 
    //tr.innerHTML = '<td>'+user.id+'</td><td>'+user.name+'</td><td>'+user.email+'</td>'

    // tr.innerHTML = '<td class="user-id"></td><td class="user-name"></td><td class="user-email"></td>'
    // tr.querySelector('.user-id').innerText = user.id;
    // tr.querySelector('.user-name').innerText = user.name;
    // tr.querySelector('.user-email').innerText = user.email;

    var tableCols = ['id', 'name', 'email']

    tableCols.forEach(function (name) {
        var td = document.createElement('td')
        tr.appendChild(td)
        td.innerText = user[name]
    })

    // znajdz tbody
    var tbody = document.querySelector('tbody')

    // dodaj wiersz do tabeli
    tbody.appendChild(tr)
}

