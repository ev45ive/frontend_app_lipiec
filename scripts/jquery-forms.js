
var form = $('form')

// Walidacja na keyup lub blur
form.on('keyup blur', 'input, textarea, select', function (event) {
    // event.target === this
    validate(form, this)
})

// Gdy formularz poprawny i wyslany (Enter lub Button)
form.on('submit', function (event) {
    var user = getFormData(form)

    saveUser(user)

    // if(!user.id){
    //     user.id = Date.now()
    // }
    // users.push(user)

    // appendUser(user)

    return false;
})

/* Walidacja pól formularza (HTML5 Form Validation) */
function validate(form, input) {

    var fields = input ? $(input) : $(form).find('input, textarea, select')

    fields.each(function (index, elem) {

        $(elem).closest('.form-group')
            .toggleClass('has-error', elem.checkValidity() == false)

    })
}

/* Pobieranie użytkownika z formularza */
function getFormData(form) {
    var user = {}
    form = $(form)

    $(form.get(0).elements)
        .filter(':checkbox:checked, :radio:checked, :not(:checkbox,:radio)')
        .each(function (index, elem) {
            user[elem.name] = $(elem).val();
        })
    return user;
}

function setFormData(form, user) {
    form = $(form)

    $(form.get(0).elements)
        .each(function (index, elem) {
            if (elem.type == 'checkbox' || elem.type == 'radio') {
                $(elem).prop('checked', elem.value == user[elem.name])
            } else {
                $(elem).val(user[elem.name])
            }
        })
}


/* Dodawanie użytkownika do tabeli */
function appendUser(user) {
    var tr = $('<tr>');
    tr.data('user_id', user.id)

    var tableCols = ['id', 'name', 'email']

    tableCols.forEach(function (name) {
        var td = $('<td>')
        td.text(user[name])
        tr.append(td)
    })
    tr.append('<td> \
        <button class="action-edit btn btn-xs btn-info"><i class="glyphicon glyphicon-pencil"></i> Edit</button>\
        <button class="action-remove btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</button>\
    </td>')

    $('tbody').append(tr)
}

/* Usuwanie użytkownika */
$('table').on('click', '.action-remove', function () {
    var id = $(this).closest('tr').data('user_id')
    deleteUser(id)
})


$('.action-new').on('click', function () {
    setFormData(form, {})
});

/* Lista użytkowników */
var users = [
    // {id:1, name: 'Zenon', email:'zenon@zeno.com'},
    // {id:2, name: 'alice', email:'alice@wonderland.com'}
]
users.forEach(appendUser) // forEach(function(user){ appendUser(user); })


/* Edycja uzytkownika */
$('table').on('click', '.action-edit', function () {
    var id = $(this).closest('tr').data('user_id');

    // var user = users.find(function (user) {
    //     return user.id == id
    // })
    fetchUser(id, function(user){
        setFormData(form, user)
        validate(form)
    })

})

function deleteUser(id){
    $.ajax({
        method: 'DELETE',
        url: 'http://localhost:3000/users/' + id,
    }).then(function (resp) {
       fetchUsers()
    });
}

function fetchUsers() {
    $('tbody').empty();

    $.getJSON('http://localhost:3000/users').then(function (resp) {
        resp.forEach(function (user) {
            users.push(user)
            appendUser(user)
        })
    })
}

function fetchUser(id, callback){
    $.getJSON('http://localhost:3000/users/' + id).then(function (user) {
        callback(user)
    })
}

function saveUser(user) {
    $.ajax({
        method: user.id? 'PUT' : 'POST',
        url: 'http://localhost:3000/users/' + (user.id || ''),
        data: user
    }).then(function (resp) {
       fetchUsers()
    });
}

fetchUsers()
